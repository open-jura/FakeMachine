#include <MemoryStats.h>
#include "main.h"

void printMemoryStats()
{
	Serial.write("H ");
	Serial.print(allocated_heap());
	Serial.write(" F ");
	Serial.println(free_stack());
}

JuraSerial<HardwareSerial> toFrogImpl(Serial3);
IJuraSerial& toFrog(toFrogImpl);

bool handlePlatformCommand(const JuraMessageView& cmd)
{
	if (strcmp(cmd.getCommand(), "po") == 0) { // module power
		char is = cmd.getFirstParameter()[0];
		if (is == '1' || !cmd.hasFirstParameter()) {
			digitalWrite(22, true);
		} else if (is == '0') {
			digitalWrite(22, false);
		}
		return true;
	}

	if (strcmp(cmd.getCommand(), "mem") == 0) { // print memory usage
		printMemoryStats();
		return true;
	}

	return false;
}

