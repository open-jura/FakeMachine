/**
 * Copyright (c) 2021-2022 Alexander Fuchs <alex.fu27@gmail.com> and the
 * contributors of the open-caffeine project.
 *
 * Licensed under the EUPL, Version 1.2 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 *
 * We are not affiliated, associated, authorized, endorsed by, or in any way
 * officially connected with Jura Elektroapparate AG. JURA and the JURA logo are
 * trademarks or registered trademarks of Jura Elektroapparate AG in Switzerland
 * and/or other countries.
 *
 * Using our software or hardware with you coffee machine may void your warranty
 * and we cannot be held liable for any damage or operating failure.
 */
#include "FakeMachine.h"
#include <JuraObfuscation.h>
#include "Arduino.h"

void FakeMachine::send(const char* message, bool silent)
{
	if (!silent) {
		debugSerial.write('<');
		if (obfuscationOn) {
			debugSerial.write('&');
		}
		debugSerial.write(message);
	}
	interface.send(message, obfuscationOn);
	if (!silent) {
		debugSerial.write("\r\n");
	}
}

void FakeMachine::send(const StringBuffer& message, bool silent)
{
	send(message.get(), silent);
}

void FakeMachine::sendFlags()
{
	StringBuffer buf(16);
	buf.append("@TF:");
	for (uint8_t i = 0; i < 5; i++) {
		buf.appendHex(flags[i], 1);
	}
	send(buf, sendFlagsVerbosity > SendFlagsVerbosity::ALL);
}

void FakeMachine::sendProgress()
{
	StringBuffer buf(64);
	buf.append("@TV:");
	const Progress* p = (Progress*) currentProgress;
	p->format(buf);
	send(buf, sendFlagsVerbosity > SendFlagsVerbosity::PROGRESS_ONLY);
}

void FakeMachine::handleOldProtocol(const JuraMessageView& msg)
{
	StringBuffer buf(128);
	if (strcmp(msg.getCommand(), "TY") == 0) {
		buf.append("ty:");
		buf.append(model);
		send(buf);
	}
}

void FakeMachine::handleSoleCommand(const JuraMessageView& msg)
{
	if (strcmp(msg.getCommand(), "T1") == 0) {
		send("@t1");
		send("@T2:01");
		return;
	}

	if (strcmp(msg.getCommand(), "T0") == 0) {
		doSendFlags = SendFlags::OFF;
		send("@t0");
		return;
	}

	if (strcmp(msg.getCommand(), "t3") == 0) {
		StringBuffer buf(128);
		obfuscationOn = true;
		doSendFlags = SendFlags::FLAGS;
		setFlag(13, true);
		return;
	}
}

void FakeMachine::handle1PCommand(const JuraMessageView& msg)
{
	StringBuffer buf(128);
	if (strcmp(msg.getCommand(), "t2") == 0) {
		buf.append("@T3:");
		buf.appendHex(modelNo, 2);
		buf.append(model);
		send(buf);
	}

	if (strcmp(msg.getCommand(), "TR") == 0) {
		buf.append("@tr:");
		buf.append(msg.getFirstParameter());
		buf.put(',');
		buf.append(register37);
		send(buf);
	}

	if (strcmp(msg.getCommand(), "TS") == 0) {
		send("@ts");
	}

	if (strcmp(msg.getCommand(), "TP") == 0) {
		send("@tp");
		startBrewing(Product::fromProductCommandParameter(msg.getFirstParameter()));
	}

	if (strcmp(msg.getCommand(), "TD") == 0) { // progress change request
		send("@td");
		debugSerial.write("progress change request: ");
		debugSerial.write(msg.getFirstParameter());
		debugSerial.write('\n');
	}

	if (strcmp(msg.getCommand(), "TG") == 0) { // progress change request
		send("@tg");
		if (strcmp(msg.getFirstParameter(), "FF") == 0) {
			// cancel product
			stopBrewing();
		}
	}
}


void FakeMachine::handle2PCommand(const JuraMessageView& msg)
{
	StringBuffer buf(128);
	if (strcmp(msg.getCommand(), "TG") == 0) {
		buf.append("@tg:");
		buf.append(msg.getFirstParameter());
		if (strcmp(msg.getFirstParameter(), "C0") == 0) {
			buf.append(maintenancePercent);
		} else if (strcmp(msg.getFirstParameter(), "43") == 0) {
			buf.append(maintenanceCounter);
		}
		send(buf);
		return;
	}
	if (strcmp(msg.getCommand(), "TR") == 0) {
		buf.append("@tr:");
		buf.append(msg.getFirstParameter());
		long reg = strtol(msg.getFirstParameter(), nullptr, 16);
		long index = strtol(msg.getSecondParameter(), nullptr, 16);
		buf.put(',');
		if (reg == 0x32) {
			buf.appendHex(index + 1, 8);
		}
		send(buf);
		return;
	}
}

void FakeMachine::handleNewProtocol(const JuraMessageView& msg)
{
	if (!msg.hasFirstParameter()) {
		handleSoleCommand(msg);
		return;
	}

	if (!msg.hasSecondParameter()) {
		handle1PCommand(msg);
		return;
	}

	handle2PCommand(msg);
}

static void outputJMV(const JuraMessageView& msg, HardwareSerial& ser)
{
	if (msg.isObfuscated()) {
		ser.write('&');
	}
	if (msg.isNewProtocol()) {
		ser.write('@');
	}
	ser.write(msg.getCommand());
	if (msg.hasFirstParameter()) {
		ser.write(':');
		ser.write(msg.getFirstParameter());
	}
	if (msg.hasSecondParameter()) {
		ser.write(',');
		ser.write(msg.getSecondParameter());
	}
}

void FakeMachine::handleMessage(const JuraMessageView& msg)
{
	obfuscationOn = msg.isObfuscated();

	debugSerial.write('>');
	outputJMV(msg, debugSerial);
	debugSerial.write('\n');

	if (msg.isNewProtocol()) {
		handleNewProtocol(msg);
	} else {
		handleOldProtocol(msg);
	}
}

FakeMachine::FakeMachine(JuraInterface& interface,
		JuraObfuscator& obfuscator, HardwareSerial& debugSerial):
	interface(interface), obfuscator(obfuscator),
	debugSerial(debugSerial),
	flags{0, 0, 0, 0, 0},
	maintenancePercent("50FF46"),
	maintenanceCounter("002D0000002C07A61DCB01F2"),
	nextStep(AutoStep::NONE)
{}

void FakeMachine::setMaintenancePercent(const char* val)
{
	maintenancePercent.assign(val);
}

void FakeMachine::setMaintenanceCounter(const char* val)
{
	maintenanceCounter.assign(val);
}

void FakeMachine::setFlagsVerbosity(SendFlagsVerbosity v)
{
	sendFlagsVerbosity = v;
}

void FakeMachine::setFlag(uint8_t index, bool value)
{
	uint8_t& the_byte = flags[index / 8];
	uint8_t bitmask = (1 << 7) >> (index % 8);
	if (value) {
		the_byte |= bitmask;
	} else {
		the_byte &= ~bitmask;
	}

	SendFlagsVerbosity fs = sendFlagsVerbosity;
	sendFlagsVerbosity = SendFlagsVerbosity::ALL;
	sendFlags();
	sendFlagsVerbosity = fs;
}

void FakeMachine::startBrewing(const Product& c)
{
	StringBuffer sb(64);
	sb.append("Starting coffee ");
	c.format(sb);
	sb.put('\n');
	debugSerial.write(sb.get());
	currentProduct = c;
	*currentProgress = BrewingProgress(currentProduct);
	BrewingProgress* p = (BrewingProgress*) currentProgress;
	p->progressCode = 0x39;
	sendFlagsNext = 0;
	doSendFlags = SendFlags::PROGRESS;
	send("@TB");
	brewState = BrewState::GRINDING;
	brewUpdateNext = millis() + 4000;
}

void FakeMachine::updateBrewing()
{
	if (brewState == BrewState::OFF)
		return;
	if (brewUpdateNext >= millis())
		return;

	BrewingProgress* p = (BrewingProgress*) currentProgress;

	switch (brewState) {
	case BrewState::GRINDING:
		brewStateExtra = -1;
		p->progressCode = 0x3C;
		debugSerial.write("grinding done\n");
		brewState = BrewState::BREWING;
		//fall through
	case BrewState::BREWING:
		brewUpdateNext = millis() + 700;
		brewStateExtra++;
		p->amountCurrent = brewStateExtra;
		if (p->amountCurrent > p->amountTarget) {
			debugSerial.write("brewing done");
			stopBrewing();
		}
		break;
	case BrewState::ENJOY:
		send("@TS");
		brewState = BrewState::OFF;
		doSendFlags = SendFlags::FLAGS;
		break;
	default:
		break;
	}
}

void FakeMachine::stopBrewing()
{
	brewState = BrewState::ENJOY;
	*currentProgress = EnjoyProgress(currentProduct);
	brewUpdateNext = millis() + 3000;
}

const Product& FakeMachine::getCurrentProduct() const
{
	return currentProduct;
}

const ProgressBuffer& FakeMachine::getCurrentProgress() const
{
	return currentProgress;
}

void FakeMachine::update()
{
	interface.update();
	if (interface.messageAvailable()) {
		JuraMessageView message(interface.getMessage());
		handleMessage(message);
	}
	if ((doSendFlags != SendFlags::OFF) && sendFlagsNext < millis()) {
		sendFlagsNext = millis() + 1000;
		if (doSendFlags == SendFlags::FLAGS) {
			sendFlags();
		} else {
			sendProgress();
		}
	}
	updateBrewing();
}

